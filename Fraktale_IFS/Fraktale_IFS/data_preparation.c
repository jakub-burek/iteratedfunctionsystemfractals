#include"header.h"

/*
Argumenty funkcji data_preparation to odpowiednio:
wska�nik na tablic� z danymi funkcji
ilo�� tych funkcji.
Funkcja ta sprawdza czy suma prawdopodobie�stw wynosi 100,
zak�adaj�c, �e tyle wynosi przestrze� zdarze� elementarnych 
a funkcje musz� wype�ni� j� w ca�o�ci.
Nast�pnie funkcja porz�dkuje elementy tablicy od najwi�kszego
do najmniejszego prawdopodobie�stwa. Nast�pnie prawdopodobie�stwem
staje si� suma prawdodpodobie�stw poprzednich funkcji do ka�dej w��cznie, 
to pozwoli p�niej losowa� funkcje zgodnie z opracowanym algorytmem.
Funkcja zwarca 0, je�eli wszystko przebieg�o prawid�owo, 1 w sytuacji przeciwnej.
*/

bool data_preparation(function* data, const int function_number)
{
	if (data == NULL || function_number <= 0)
		return 1;

	double sum = 0;
	for (int i = 0; i < function_number; i++)
		sum = sum + data[i].probability;

	if (sum != 100)
        return 1;

	function* temp = malloc(sizeof(function));

	for (int i = 0; i < function_number; i++)
	{
		for (int j = (i + 1); j < function_number; j++)
		{
			if (data[i].probability < data[j].probability)
			{
				*temp = data[i];
				data[i] = data[j];
				data[j] = *temp;
			}
		}
	}

	free(temp);

	for (int i = 1; i < function_number; i++)
		data[i].probability = data[i].probability + data[i - 1].probability;

    return 0;
}

/*
Funkcja data_printf pobiera jako argumenty dane funkcji oraz ich ilo��.
Wypisuje ilo�� funkcji, nast�pnie rz�dami ich parametry oraz g�rny limit prawdopodobie�stwa.
Funkcja nie zwraca �adnej warto�ci.
*/

bool data_printf(const function* data, const int function_number)
{
	if (data == NULL || function_number <= 0)
		return 1;
	
	printf("Number of functions: %d.\n", function_number);
	printf("Function's parameters and probability's upper limit: \n");

	for (int i = 0; i < function_number; i++)
	{
		for (int j = 0; j < param_count; j++)
			printf("%lf ", data[i].parametrs[j]);
		
		printf("%lf\n", data[i].probability);
	}

	return 0;
}