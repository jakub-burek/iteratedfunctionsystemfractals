#include"header.h"

/*
Argumenty funkcji command line to kolejno:
ilo�� parametr�w wiersza polce�, parametry wiersza polece�,
wska�nik na identyfikator pliku z danymi wej�ciowymi oraz wska�nik na liczb� iteracji.
Program odczytuje po odpowiednich prze��cznikach:
-d nazw� pliku wej�ciowego i sprawdza czy mo�na go otworzy� do odczytu
-i liczb� iteracji
Je�eli plik mo�na otworzy� i oba parametry z lini polece� s� prawid�owe funkcja zwraca 0,
w przeciwnym razie zwraca 1.
*/

bool command_line(int argc, char**argv, FILE** input_file, int* iterations)
{
	if (input_file == NULL || iterations == NULL)
		return 1;

	int file_check = 0;
	int iterations_check = 0;

	for (int i = 1; i < (argc - 1); ++i)
	{
		if (strcmp(argv[i], "-d") == 0)
		{
			if (++i <= argc && file_check == 0)
			{
				*input_file = fopen(argv[i], "rb");
				if (*input_file == NULL)
					return 1;
				else
					++file_check;
			}
			else
				return 1;
		}
		if (strcmp(argv[i], "-i") == 0)
		{
			if (++i <= argc)
			{
				*iterations = atoi(argv[i]);

				if (*iterations <= 0)  
					return 1;
				else
					++iterations_check;
			}
			else
				return 1;
		}
	}

	if (file_check == 1 && iterations_check == 1)
		return 0;
	else
		return 1;
}