#include"header.h"

/*
Funkcja select_function korzysta z:
danych funkcji,
ilo�� funkcji,
wylosowanej liczby z odpowiedniego zakresu.
Wybiera, kt�r� funkcj� nale�y u�y� w kolejnej iteracji.
Funkcja zwraca liczb� porz�dkow� funkcji zawartej w tablicy danych funkcji,
Je�eli ta operacja nie powiedzie si� funkcja zwraca waro�� ujemn�.
*/


int select_function(const function* data, const int function_number, const int nrand)
{
	if (data == NULL || function_number <= 0)
		return -1;

	if (nrand < data[0].probability)
		return 0;

	for (int i = 1; i < function_number; ++i)
	{
		if (nrand >= data[i - 1].probability && nrand < data[i].probability)
			return i;
	}
	return -1;
}

/*
Argumenty funkcji generate_point to:
wska�nik na struktur� przechowuj�c� wsp�rz�dne,
dane funkcji,
ilo�� funkcji.
Funkcja losuje dowoln� warto�� z przestrzeni zdarze� elementarnych,
nast�pnie sprawdza kt�r� funkcje nale�y wykorzysta�.
Maj�c te informacje dokonuje iteracji, zmiany wsp�rz�dnych.
Funkcja zwraca warto�� 0 je�li jej dzia�anie przebieg�o prawid�owo,
w przeciwnym razie zwaraca 1.
*/

bool generate_point(point_data* point, const function* data, const int function_number)
{
    if (point == NULL || data == NULL || function_number <= 0 )
        return 1;

    const int random_number = rand() % 100;
    const int choice = select_function(data, function_number, random_number);
	if (choice < 0)
		return 1;

    const point_data previous_point = *point;
    point->x = (data[choice].parametrs[a] * previous_point.x) + (data[choice].parametrs[b] * previous_point.y) + data[choice].parametrs[c];
    point->y = (data[choice].parametrs[d] * previous_point.x) + (data[choice].parametrs[e] * previous_point.y) + data[choice].parametrs[f];

    return 0;
}

/*
Funkcja calculate_extreme_points korzysta z:
wska�nika na struktut�, w kt�rej zapisze rozmiar i po�o�enie fraktala,
ilo�ci iteracji testowych, kt�re pozwol� obliczy� wspomniane warto�ci,
danych funkcji,
oraz ilo�ci funckcji.
Funkcja oblicza najbardziej skrajne wsp�rz�dne fraktala.
Nast�pnie przkazuje do struktury extreme_points wsp�rz�dne
dolnego-lewego punktu oraz rozmiary fraktala - 
r�nic� mi�dzy skrajnymi wsp�rz�dnymi wzd�u� obu osi.
Funkcja zwraca warto�� 0 je�li jej dzia�anie przebieg�o prawid�owo,
w przeciwnym razie zwaraca 1.
*/

bool calculate_extreme_points(fractal_points* extreme_points, const int iterations, const function* data, const int function_number)
{
    if (extreme_points == NULL || data == NULL || function_number <= 0 || iterations <= 0 )
        return 1;

	point_data left_bottom = { DBL_MAX, DBL_MAX };
	point_data right_top = { -DBL_MAX, -DBL_MAX };
	point_data current_point = { 0, 0};

    for (int i = 0; i < iterations; ++i)
    {
        if (generate_point(&current_point, data, function_number) != 0)
            return 1;

        if (current_point.x < left_bottom.x)
            left_bottom.x = current_point.x;

        if (current_point.y < left_bottom.y)
            left_bottom.y = current_point.y;

        if (current_point.x > right_top.x)
            right_top.x = current_point.x;

        if (current_point.y > right_top.y)
            right_top.y = current_point.y;
    }

    extreme_points->position = left_bottom;
    extreme_points->size.x = right_top.x - left_bottom.x;
    extreme_points->size.y = right_top.y - left_bottom.y;

    return 0;
}

/*
Argumenty funkcji fractal_generation to:
wska�nik na tablic� z danymi funkcji,
ilo�� funkcji,
ilo�� iteracji.
Funkcja ta korzysta z biblioteki Allegro. Inicjuje ona jej dzia�anie,
obs�ug� myszy i klawiatury. Nast�pnie Tworzone jest okno, nadaje si� mu nazw�,
umieszcza si� w nim pust� bitmap�, kt�ra wype�nia je w ca�o�ci.
Nast�pnie obliczone zostaj� rozmiar i pozycja fraktala.
Na tej podstawie obliczona jest skala oraz przesuni�cie jakie nale�y odj�� 
od wyskalowanych wsp�rz�dnych, aby fraktal wype�nia� �rodek okna.
Po wygenerowaniu wszystkich punkt�w fraktal jest pokazywany w okienku.
Po naci�ni�ciu przycisku escape fraktal i okno s� usuwane, a biblioteka Allegro jest dezaktywowana.
Funkcja zwraca warto�� 0, je�eli wszystko przebieg�o prawid�owo, w przeciwnym razie zwraca warto�� 1.
*/

bool fractal_generation(const function* data, const int function_number, const int iterations)
{
	if (data == NULL || function_number <= 0 || iterations <= 0)
		return 1;

	al_init();
    al_install_keyboard();
   
    const SIZE screen_size = { 500, 500};

	al_set_new_window_position(500, 200);
	al_set_new_display_flags(ALLEGRO_WINDOWED);
	ALLEGRO_DISPLAY* window = al_create_display(screen_size.cx, screen_size.cy);
	al_set_window_title(window, "Fractal");
	ALLEGRO_BITMAP *fractal = al_create_bitmap(screen_size.cx, screen_size.cy);

	al_set_target_bitmap(fractal);
	al_clear_to_color(al_map_rgb(255, 255, 255));
	al_set_target_bitmap(al_get_backbuffer(window));
	al_draw_bitmap(fractal, 0, 0, 0);
	al_flip_display();

    fractal_points extreme_points;

	if (calculate_extreme_points(&extreme_points, 10000, data, function_number) != 0)
		return 1;

	if (extreme_points.size.x <= 0 || extreme_points.size.y <= 0)
		return 1;

    const double scale_x = ((double)screen_size.cx / extreme_points.size.x);
    const double scale_y = ((double)screen_size.cy / extreme_points.size.y);

    const double scale = min(scale_x, scale_y);

	point_data offset;
    offset.x = (extreme_points.position.x + extreme_points.size.x / 2.0) * scale - screen_size.cx / 2;
    offset.y = (extreme_points.position.y + extreme_points.size.y / 2.0) * scale - screen_size.cy / 2;
    
	point_data current_point = { 0, 0};

	for (int i = 0; i < iterations; ++i)
	{
        generate_point(&current_point, data, function_number);
        al_draw_pixel(current_point.x * scale - offset.x, current_point.y * scale - offset.y, al_map_rgb(0, 0, 0));
	}

    ALLEGRO_KEYBOARD_STATE keyboard_state;

	al_flip_display();
    do
    {
        Sleep(10);
        al_get_keyboard_state(&keyboard_state);
    }
    while (al_key_down(&keyboard_state, ALLEGRO_KEY_ESCAPE) == 0);
	al_destroy_bitmap(fractal);
	al_destroy_display(window);
	al_uninstall_system();
    return 0;
}