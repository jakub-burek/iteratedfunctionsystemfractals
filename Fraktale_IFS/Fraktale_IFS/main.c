#include"header.h"

int main(int argc, char**argv)
{
	FILE* input_file = NULL;
	int iterations = 0;

	if (command_line(argc, argv, &input_file, &iterations) != 0)
	{
		printf("Incorrect CMD line arguments or input data.\n");
		return 1;
	}

	int function_number = 0;
	function * data = file_extraction(input_file, &function_number);
	if (data == NULL)
	{
		printf("Data reading error.\n");
		fclose(input_file);
		return 1;
	}
	fclose(input_file);

    if (data_preparation(data, function_number) != 0)
    {
        printf("Incorrect data, check file. \n");
		free(data);
        return 1;
    }

	if (data_printf(data, function_number) != 0)
	{
		printf("Data printing error. \n");
		free(data);
		return 1;
	}

	srand((unsigned int)time(NULL));
	if (fractal_generation(data, function_number, iterations) != 0)
	{
		printf("Fractal generation error. \n");
		free(data);
		return 1;
	}

	free(data);
    return 0;
}