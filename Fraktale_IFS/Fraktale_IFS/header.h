#pragma once //Do projektu do��czane s� odpowiednie pliki �r�d�owe.
#include<stdio.h>
#include<stdlib.h>
#include <time.h>
#include <ctype.h>
#include <string.h>
#include <allegro5\allegro.h>
#include<Windows.h>
#include<float.h>

//Ilo�� parametr�w funkcji
#define param_count 6 

//Struktura przechowuj�ca parametry funkcji oraz prawdopodie�stwo jej wyst�pienia.
typedef struct  { 
	double parametrs[param_count];
	double probability;
} function;

//Typ wyliczeniowy pozwalaj�cy rozr�ni� parametry u�ywanych funkcji.
typedef enum { 
	a, b, c, d, e, f
}p;

//Struktura przechowuj�ca wsp�rz�dne punktu.
typedef struct
{
	double x;
	double y;
} point_data;

//Struktura przechowuj�ca dwa punkty: pozycj� i rozmiar fraktala.
typedef struct
{
	point_data position;
	point_data size;
} fractal_points;

//Deklaracje funkcji z kt�rych korzysta program.
bool command_line(int argc, char**argv, FILE** input_file, int* iterations);
function* file_extraction(FILE* input_file, int* function_number);
bool data_preparation(function* data, const int function_number);
bool data_printf(const function* data, const int function_number);
bool fractal_generation(const function* data, const int function_number, const int iterations);
bool calculate_extreme_points(fractal_points* extreme_points, const int iterations, const function* data, const int function_number);
bool generate_point(point_data* point, const function* data, const int function_number);
int select_function(const function* data, const int function_number, const int nrand);