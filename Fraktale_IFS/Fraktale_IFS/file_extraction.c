#include"header.h"

/*
Argumenty funkcji file_extraction to identyfikator pliku wej�ciowego oraz
wska�nik na liczb� funkcji odczytywanych z tego pliku.
Funkcja ta najpierw pobiera warto�� typu int- ilo�� funkcji,
a nast�pnie odczytuje warto�ci typu double 6 parametr�w praz prwadopodobie�stwo.
Je�eli dane wej�ciowe b�d� nieprawid�wo sformatowane funkcja wy�wietli komunikat
i zako�czy dzia�anie programu. W przeciwnym razie zwr�ci ona wska�nik na tablic� 
z danymi funkcji oraz przypisze ilo�� funkcji drugiemu argumentowi.
*/

function* file_extraction(FILE* input_file, int* function_number)
{
	if (input_file == NULL || function_number == NULL)
		return NULL;

	if (fscanf(input_file, "%d", function_number) != 1)
		return NULL;
	if (*function_number <= 0)
		return NULL;

    function* data = (function*)malloc((*function_number) * sizeof(function));

	for (int i = 0; i < (*function_number); i++)
	{
		for (int j = 0; j < param_count; j++)
		{
			if (fscanf(input_file, "%lf", &data[i].parametrs[j]) != 1)
			{
				free(data);
				return NULL;
			}
		}
		if (fscanf(input_file, "%lf", &data[i].probability) != 1)
		{
			free(data);
			return NULL;
		}
		data[i].probability = 100 * data[i].probability;
	}

	return data;
}